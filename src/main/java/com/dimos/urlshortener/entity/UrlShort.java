package com.dimos.urlshortener.entity;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDateTime;

@Entity
public class UrlShort {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String physicalUrl;

    private String urlCode;

    @CreationTimestamp
    private LocalDateTime createdTimeStamp;

    public UrlShort(String originalUrl) {
        this.physicalUrl = originalUrl;
    }

    public UrlShort(){}

    public long getId() {
        return id;
    }

    public String getPhysicalUrl() {
        return physicalUrl;
    }

    public void setPhysicalUrl(String physicalUrl) {
        this.physicalUrl = physicalUrl;
    }

    public String getUrlCode() {
        return urlCode;
    }

        public void setUrlCode(String urlCode) {
        this.urlCode = urlCode;
    }

    public LocalDateTime getCreatedTimeStamp() {
        return createdTimeStamp;
    }

    public void setCreatedTimeStamp(LocalDateTime createdTimeStamp) {
        this.createdTimeStamp = createdTimeStamp;
    }
}
