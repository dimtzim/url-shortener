package com.dimos.urlshortener.repository;

import com.dimos.urlshortener.entity.UrlShort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UrlShortRepo extends JpaRepository<UrlShort, Long> {

    Optional<UrlShort> findByUrlCode(String urlCode);
}
