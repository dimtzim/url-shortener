package com.dimos.urlshortener.dto;

import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.NotEmpty;

public class ShortenerUrlDto {

    @NotEmpty(message = "{val.err.shorten.url.not.empty.or.null}")
    @URL(message = "{val.err.shorten.url.invalid}")
    private String shortenUrl;

    public ShortenerUrlDto() {
    }

    public ShortenerUrlDto(String shortenUrl) {
        this.shortenUrl = shortenUrl;
    }

    public String getShortenUrl() {
        return shortenUrl;
    }

    public void setShortenUrl(String shortenUrl) {
        this.shortenUrl = shortenUrl;
    }
}
