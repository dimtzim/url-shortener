package com.dimos.urlshortener.controller;

import com.dimos.urlshortener.dto.OriginalUrlDto;
import com.dimos.urlshortener.dto.ShortenerUrlDto;
import com.dimos.urlshortener.service.impl.UrlShortenerServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/url")
@CrossOrigin(origins = "http://localhost:8081")
public class UrlShorteningController {

    @Autowired
    private UrlShortenerServiceImpl urlShortenerService;

    @PostMapping("/shortener")
    public ResponseEntity<ShortenerUrlDto> shortenUrl(@Validated @RequestBody OriginalUrlDto originalUrlDto) {
        return ResponseEntity.ok(new ShortenerUrlDto(urlShortenerService.shortenUrl(originalUrlDto.getOriginalUrl())));
    }
}
