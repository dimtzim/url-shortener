package com.dimos.urlshortener.controller;

import com.dimos.urlshortener.service.impl.UrlShortenerServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/tiny")
@CrossOrigin(origins = "http://localhost:8081")
public class UrlRedirectController {

    @Autowired
    private UrlShortenerServiceImpl urlShortenerService;

    @GetMapping("/{shortenStr}")
    public ModelAndView redirectToOriginal(@PathVariable("shortenStr") String shortUrl) {
        return urlShortenerService.getShortenUrl(shortUrl)
                .map(shortenUrl -> new ModelAndView("redirect:" + shortenUrl.getPhysicalUrl()))
                .orElseGet(() -> new ModelAndView("tiny not found"));
    }
}
