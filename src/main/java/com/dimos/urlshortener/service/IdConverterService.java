package com.dimos.urlshortener.service;

public interface IdConverterService {

    String encode(long num);

    long decode(String shortKey);
}
