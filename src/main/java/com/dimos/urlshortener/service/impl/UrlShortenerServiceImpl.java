package com.dimos.urlshortener.service.impl;

import com.dimos.urlshortener.entity.UrlShort;
import com.dimos.urlshortener.repository.UrlShortRepo;
import com.dimos.urlshortener.service.UrlShortenerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UrlShortenerServiceImpl implements UrlShortenerService {

    @Value("${base.url}")
    private String BASE_URL;

    @Value("${shorten.url.prefix}")
    private String SHORTEN_URL_PREFIX;

    @Autowired
    private IdConverterServiceImpl idConverterService;

    @Autowired
    private UrlShortRepo urlShortRepo;

    @Override
    public String shortenUrl(String originalUrl) {
        return BASE_URL.concat(SHORTEN_URL_PREFIX).concat(urlShortRepo.findByUrlCode(originalUrl)
        .orElseGet(() -> saveAndShorten(originalUrl)).getUrlCode());

    }

    private UrlShort saveAndShorten(String originalUrl) {
        UrlShort url = urlShortRepo.save(new UrlShort(originalUrl));
        String shortenKey = idConverterService.encode(url.getId());

        url.setUrlCode(shortenKey);

        return url;
    }

    @Override
    public Optional<UrlShort> getShortenUrl(String shortUrl) {
        return urlShortRepo.findById(idConverterService.decode(shortUrl));
    }
}
