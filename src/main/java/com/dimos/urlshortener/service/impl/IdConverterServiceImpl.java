package com.dimos.urlshortener.service.impl;

import com.dimos.urlshortener.service.IdConverterService;
import org.springframework.stereotype.Service;

@Service
public class IdConverterServiceImpl implements IdConverterService {

    private static final String POSSIBLE_ALPHABET = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    private static final long BASE = POSSIBLE_ALPHABET.length();

    @Override
    public String encode(long id) {
        StringBuilder shortenKey = new StringBuilder();
        while (id > 0) {
            shortenKey.insert(0, POSSIBLE_ALPHABET.charAt((int) (id % BASE)));
            id = id / BASE;
        }
        return shortenKey.toString();
    }

    @Override
    public long decode(String shortKey) {
        long num = 0;
        for (int i = 0; i < shortKey.length(); i++) {
            num = num * BASE + POSSIBLE_ALPHABET.indexOf(shortKey.charAt(i));
        }
        return num;
    }
}
