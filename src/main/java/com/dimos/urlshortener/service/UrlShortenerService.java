package com.dimos.urlshortener.service;

import com.dimos.urlshortener.entity.UrlShort;

import java.util.Optional;

public interface UrlShortenerService {

    String shortenUrl(String originalUrl);

    Optional<UrlShort> getShortenUrl(String shortUrl);
}
